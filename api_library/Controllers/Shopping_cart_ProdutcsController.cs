﻿using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api/scp")]
    [ApiController]
    public class Shopping_cart_ProdutcsController : ControllerBase
    {
        private IShopping_cart_ProdutcsService shopping_Cart_ProdutcsService;
        public Shopping_cart_ProdutcsController(IShopping_cart_ProdutcsService _shopping_Cart_ProdutcsService)
        {
            this.shopping_Cart_ProdutcsService = _shopping_Cart_ProdutcsService;
        }

        // GET all scp
        [HttpGet]
        public async Task<ActionResult> Get_SCP()
        {
            var respuesta = new RespuestaListaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Get_SCP();
            return Ok(respuesta);
        }

        // GET scp bry p sc id
        [HttpGet("{idSc:int}")]
        public async Task<ActionResult> Get_SC_P_By_SCID(int idSc)
        {
            var respuesta = new RespuestaListaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Get_SC_P_By_SCID(idSc);
            return Ok(respuesta);
        }

        // GET scp bry p id
        [HttpGet("productId/{idP:int}")]
        public async Task<ActionResult> Get_SC_P_By_PID(int idP)
        {
            var respuesta = new RespuestaListaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Get_SC_P_By_PID(idP);
            return Ok(respuesta);
        }

        [HttpGet("{idSC:int}/productId/{idP:int}")]
        public async Task<IActionResult> Get_SC_P_By_SC_P_ID(int idSC, int idP)
        {
            var respuesta = new RespuestaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Get_SC_P_By_SC_P_ID(idSC, idP);
            return Ok(respuesta);
        }

        // Crear
        [HttpPost("{idSC:int}/productId/{idP:int}")]
        public async Task<IActionResult> Create_SCP(int idSC, int idP)
        {
            var respuesta = new RespuestaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Create_SCP(idSC, idP);
            return Ok(respuesta);
        }

        //update
        [HttpPut]
        public async Task<IActionResult> Update_cantProductSCP([FromBody] Shopping_cart_Produtcs scp, int cant)
        {
            var respuesta = new RespuestaShopping_cart_Produtcs();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Update_cantProductSCP(scp, cant);
            return Ok(respuesta);
        }

        // DELETE
        [HttpDelete]
        public async Task<ActionResult<bool>> Delete_P_By_scp([FromBody] Shopping_cart_Produtcs scp)
        {
            var respuesta = new RespuestaBoolean();
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_Cart_ProdutcsService.Delete_P_By_scp(scp);
            return Ok(respuesta);
        }
    }
}

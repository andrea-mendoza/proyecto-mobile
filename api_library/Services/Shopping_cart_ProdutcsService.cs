﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Entities;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class Shopping_cart_ProdutcsService : IShopping_cart_ProdutcsService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public Shopping_cart_ProdutcsService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "id" };
        }
        public async Task<Shopping_cart_Produtcs> Create_SCP(int idSC, int idP)
        {
            var scp = libraryRepository.Create_SCP(idSC, idP);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Shopping_cart_Produtcs>(scp);
            }
            throw new Exception("There were an error with the DB");
        }
        public async Task<bool> Delete_P_By_scp(Shopping_cart_Produtcs scp)
        {
            //await validateLibrary(id);
            var scpToDelete = mapper.Map<Shopping_cart_ProdutcsEntity>(scp);
            await libraryRepository.Delete_P_By_scp(scpToDelete);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }
        public async Task<bool> Delete_SCP_By_SCID(int idSC)
        {
            //await validateLibrary(id);
            await libraryRepository.Delete_SCP_By_SCID(idSC);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<Shopping_cart_Produtcs>> Get_SCP()
        {
            var scpEntities = await libraryRepository.Get_SCP();
            return mapper.Map<IEnumerable<Shopping_cart_Produtcs>>(scpEntities);
        }
        public async Task<IEnumerable<Shopping_cart_Produtcs>> Get_SC_P_By_PID(int idP)
        {
            var scpEntities = await libraryRepository.Get_SC_P_By_PID(idP);
            return mapper.Map<IEnumerable<Shopping_cart_Produtcs>>(scpEntities);
        }
        public async Task<IEnumerable<Shopping_cart_Produtcs>> Get_SC_P_By_SCID(int idSC)
        {
            var scpEntities = await libraryRepository.Get_SC_P_By_SCID(idSC);
            return mapper.Map<IEnumerable<Shopping_cart_Produtcs>>(scpEntities);
        }
        public async Task<Shopping_cart_Produtcs> Get_SC_P_By_SC_P_ID(int idSC, int idP)
        {
            var scp = await libraryRepository.Get_SC_P_By_SC_P_ID(idSC, idP);

            if (scp == null)
            {
                throw new NotFoundItemException("library not found");
            }

            return mapper.Map<Shopping_cart_Produtcs>(scp);
        }
        public async Task<Shopping_cart_Produtcs> Update_cantProductSCP(Shopping_cart_Produtcs scp, int cant)
        {
            var scpEntity = mapper.Map<Shopping_cart_ProdutcsEntity>(scp);

            libraryRepository.Update_cantProductSCP(scpEntity, cant);
            if (await libraryRepository.SaveChangesAsync())
            {
                return await Get_SC_P_By_SC_P_ID(scp.cartID, scp.cartID);
            }

            throw new Exception("There were an error with the DB");
        }
    }
}

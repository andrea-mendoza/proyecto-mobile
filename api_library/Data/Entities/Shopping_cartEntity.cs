﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Entities
{
    public class Shopping_cartEntity
    {
        [Key]
        [Required]

        public int? id { get; set; }
        [Required]

        public int id_user { get; set; }
        public ICollection<Shopping_cart_ProdutcsEntity> shopping_Cart_Produtcs { get; set; }
        public int total_cost { get; set; }
    }
}

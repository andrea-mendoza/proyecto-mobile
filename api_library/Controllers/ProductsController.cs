﻿using api_library.Exceptions;
using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api")]
    public class ProductsController : ControllerBase
    {

        private IProductsService productsService;

        public ProductsController(IProductsService productsService)
        {
            this.productsService = productsService;
        }

        [HttpGet("{products}")]
        public async Task<ActionResult> GetProducts(string orderBy = "id")
        {
            var respuesta = new RespuestaListaProducto();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await productsService.GetProductsAsync(orderBy);
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }

        [HttpGet("products/{id}")]
        public async Task<IActionResult> GetProductAsync(int id)
        {
            var respuesta = new RespuestaProducto();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await productsService.GetProductAsync(id);
                return Ok(respuesta);

            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }

        }

        [HttpPost("library/{idLibrary:int}/products")]
        public async Task<IActionResult> PostProduct(int idLibrary, [FromBody] Product product)
        {
            var respuesta = new RespuestaProducto();
            if (!ModelState.IsValid)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, llene los espacios correctamente.";
            }
            else
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await productsService.CreateProductAsync(idLibrary, product);
            }
            return Ok(respuesta);
        }


        [HttpDelete("products/{Id:int}")]
        public async Task<ActionResult<bool>> DeleteProduct(int Id)
        {
            var respuesta = new RespuestaBoolean();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await this.productsService.DeleteProductAsync(Id);
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
        [HttpPut("products/{id:int}")]
        public async Task<IActionResult> PutProduct(int id, [FromBody]Product product)
        {
            var respuesta = new RespuestaProducto();
            if (!ModelState.IsValid)
            {
                var nameproduct= ModelState[nameof(product.name)];
                if (nameproduct != null && nameproduct.Errors.Any())
                {
                    respuesta.code = 404;
                    respuesta.messsage = "Sin Exito, no encontrado";
                    return Ok(respuesta);
                }
            }
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await productsService.UpdateProductAsync(id, product);
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
        }


        [HttpGet("library/{idLibrary:int}/products")]
        public async Task<ActionResult> GetProductsByIdLibrary(int idLibrary)
        {
            var respuesta = new RespuestaListaProducto();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await productsService.GetProductsByLibraryId(idLibrary);
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
    }
}

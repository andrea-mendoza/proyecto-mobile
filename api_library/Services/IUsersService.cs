﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface IUsersService
    {
        Task<IEnumerable<User>> GetUsersById_rol(int id_rol);
        Task<IEnumerable<User>> GetUserAsync( string orderBy);
        Task<User> GetUserAsync(int id);
        Task<User> CreateUserAsync(User newUser);
        Task<bool> DeleteUserAsync(int id);
        Task<bool> findUserByEmailPassword(string email, string password);
        Task<User> UpdateUserAsync(int id, User newUser);
    }
}

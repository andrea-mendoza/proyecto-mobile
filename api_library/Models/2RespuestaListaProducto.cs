﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class RespuestaListaProducto
    {
        public int code { get; set; }

        public string messsage { get; set; }

        public IEnumerable<Product> result { get; set; }
    }
}

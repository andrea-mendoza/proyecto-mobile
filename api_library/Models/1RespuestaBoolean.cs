﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class RespuestaBoolean
    {
        public int code { get; set; }

        public string messsage { get; set; }

        public Boolean result { get; set; }
    }
}

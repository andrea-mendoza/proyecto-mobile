﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface IShopping_cart_ProdutcsService
    {
        Task<Shopping_cart_Produtcs> Get_SC_P_By_SC_P_ID(int idSC, int idP);
        Task<IEnumerable<Shopping_cart_Produtcs>> Get_SC_P_By_SCID(int idSC);
        Task<IEnumerable<Shopping_cart_Produtcs>> Get_SC_P_By_PID(int idP);
        Task<IEnumerable<Shopping_cart_Produtcs>> Get_SCP();
        Task<bool> Delete_SCP_By_SCID(int idSC);
        Task<bool> Delete_P_By_scp(Shopping_cart_Produtcs scp);
        Task<Shopping_cart_Produtcs> Update_cantProductSCP(Shopping_cart_Produtcs scp, int cant);
        Task<Shopping_cart_Produtcs> Create_SCP(int idSC, int idP);
    }
}

﻿using api_library.Exceptions;
using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api/[controller]")]
    public class LibrarysController : ControllerBase
    {

        private ILibraryService librarysService;

        public LibrarysController(ILibraryService librarysService)
        {
            this.librarysService = librarysService;
        }

        [HttpGet]
        public async Task<ActionResult> GetLibrarys( string orderBy = "id")
        {
            var respuesta = new RespuestaListaLibrary();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await librarysService.GetLibrarysAsync( orderBy);
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLibraryAsync(int id)
        {
            var respuesta = new RespuestaLibrary();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await librarysService.GetLibraryAsync(id);
                return Ok(respuesta);

            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }

        }
        [HttpPost]
        public async Task<IActionResult> PostLibrary([FromBody] Library library)
        {
            var respuesta = new RespuestaLibrary();
            if (!ModelState.IsValid)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, llene los espacios correctamente.";
            }
            else
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await librarysService.CreateLibraryAsync(library);
            }
            return Ok(respuesta);
        }
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<bool>> DeleteLibrary(int Id)
        {
            var respuesta = new RespuestaBoolean();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = (await this.librarysService.DeleteLibraryAsync(Id));
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutLibrary(int id, [FromBody]Library library)
        {
            var respuesta = new RespuestaLibrary();
            if (!ModelState.IsValid)
            {
                var namelibrary = ModelState[nameof(library.name)];
                if (namelibrary != null && namelibrary.Errors.Any())
                {
                    respuesta.code = 404;
                    respuesta.messsage = "Sin Exito, no encontrado";
                    return Ok(respuesta);
                }
            }

            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = (await librarysService.UpdateLibraryAsync(id, library));
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
        }
    }
}

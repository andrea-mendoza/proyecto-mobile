﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class Product
    {
        public int? id { get; set; }
        [Required]

        public string name { get; set; }
        public int price { get; set; }
        public string photo { get; set; }
        public int quatity { get; set; }
        public bool state { get; set; }
        public int id_library { get; set; }

        public IList<Shopping_cart> carts { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class RespuestaListaShopping_cart
    {
        public int code { get; set; }

        public string messsage { get; set; }

        public IEnumerable<Shopping_cart> result { get; set; }
    }
}

﻿using api_library.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Library
            modelBuilder.Entity<LibraryEntity>().ToTable("Librarys");
            modelBuilder.Entity<LibraryEntity>().Property(a => a.id).ValueGeneratedOnAdd();
            //Rol
            modelBuilder.Entity<RolEntity>().ToTable("Rols");
            modelBuilder.Entity<RolEntity>().Property(a => a.id).ValueGeneratedOnAdd();
            //User
            modelBuilder.Entity<UserEntity>().ToTable("Users");
            modelBuilder.Entity<UserEntity>().Property(a => a.id).ValueGeneratedOnAdd();

            //Shopping_cart-Product
            modelBuilder.Entity<Shopping_cart_ProdutcsEntity>().ToTable("Shopping_carts-Products");
            modelBuilder.Entity<Shopping_cart_ProdutcsEntity>().HasKey(cp => new { cp.cartID, cp.productID });
            modelBuilder.Entity<Shopping_cart_ProdutcsEntity>().HasOne(cp => cp.product).WithMany(p => p.shopping_Cart_Produtcs).HasForeignKey(cp => cp.productID);
            modelBuilder.Entity<Shopping_cart_ProdutcsEntity>().HasOne(cp => cp.shopping_Cart).WithMany(c => c.shopping_Cart_Produtcs).HasForeignKey(cp => cp.cartID);

            //Product
            modelBuilder.Entity<ProductEntity>().ToTable("Products");
            modelBuilder.Entity<ProductEntity>().Property(b => b.id).ValueGeneratedOnAdd();

            //Shopping_cart
            modelBuilder.Entity<Shopping_cartEntity>().ToTable("Shopping_carts");
            modelBuilder.Entity<Shopping_cartEntity>().Property(b => b.id).ValueGeneratedOnAdd();

        }

        public DbSet<LibraryEntity> Librarys { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<RolEntity> Rols { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<Shopping_cartEntity> Shopping_Carts { get; set; }
        public DbSet<Shopping_cart_ProdutcsEntity> Shopping_Cart_Produtcs {get;set;}
    }   
}

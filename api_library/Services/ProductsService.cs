﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Entities;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class ProductsService : IProductsService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public ProductsService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "lastname", "age", "id" };
        }

        public async Task<Product> CreateProductAsync(int idLibrary, Product newProduct)
        {
            newProduct.id_library = idLibrary;
            var librariesEntity = await libraryRepository.GetLibrarysAsync();
            var libraries = mapper.Map<IEnumerable<Library>>(librariesEntity);
            if (libraries.ToList().Exists(l => l.id == idLibrary))
            {

                var productEntity = mapper.Map<ProductEntity>(newProduct);
                libraryRepository.CreateProduct(productEntity);
                if (await libraryRepository.SaveChangesAsync())
                {
                    return mapper.Map<Product>(productEntity);
                }
                throw new Exception("There were an error with the DB");
            }
            else
            {
                throw new Exception("There were an error with the DB");
            }
        }

        public async Task<bool> DeleteProductAsync(int id)
        {
            await validateProduct(id);
            await libraryRepository.DeleteProduct(id);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }

        public async Task<Product> GetProductAsync(int id)
        {

            var product = await libraryRepository.GetProductAsync(id);

            if (product == null)
            {
                throw new NotFoundItemException("product not found");
            }

            return mapper.Map<Product>(product);

        }

        public async Task<IEnumerable<Product>> GetProductsAsync(string orderBy)
        {
            var orderByLower = orderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {orderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var productsEntities = await libraryRepository.GetProducts();
            return mapper.Map<IEnumerable<Product>>(productsEntities);
        }

        public async Task<Product> UpdateProductAsync(int id, Product product)
        {
            if (id != product.id)
            {
                throw new InvalidOperationException("URL id needs to be the same as Author id");
            }
            await validateProduct(id);

            product.id = id;
            var productEntity = mapper.Map<ProductEntity>(product);
            libraryRepository.UpdateProduct(productEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Product>(productEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        private async Task<ProductEntity> validateProduct(int id)
        {
            var product = await libraryRepository.GetProductAsync(id);
            if (product == null)
            {
                throw new NotFoundItemException($"cannot found author with id {id}");
            }

            return product;
        }

        public async Task<IEnumerable<Product>> GetProductsByLibraryId(int idLibrary)
        {
            var productsEntities = await libraryRepository.GetProducts();

            return mapper.Map<IEnumerable<Product>>(productsEntities.Where(p => p.id_library == idLibrary));
        }
    }
}

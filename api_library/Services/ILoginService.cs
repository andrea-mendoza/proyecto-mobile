using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface ILoginService
    {
        string decoder(string code);
        Task<bool> verify(string decode);
    }
}

﻿using api_library.Exceptions;
using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUsersService usersService;

        public UsersController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet]
        public async Task<ActionResult> GetUsers(string orderBy = "id")
        {
            var respuesta = new RespuestaListaUser();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await usersService.GetUserAsync(orderBy);
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserAsync(int id)
        {
            var respuesta = new RespuestaUser();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await usersService.GetUserAsync(id);
                return Ok(respuesta);

            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }

        }
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            var respuesta = new RespuestaUser();
            if (!ModelState.IsValid)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }

            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await usersService.CreateUserAsync(user);
            return Ok(respuesta);
        }

        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<bool>> DeleteUser(int Id)
        {
            var respuesta = new RespuestaBoolean();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await this.usersService.DeleteUserAsync(Id);
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutUser(int id, [FromBody]User user)
        {
            var respuesta = new RespuestaUser();
            if (!ModelState.IsValid)
            {
                var name = ModelState[nameof(user.name)];

                if (name != null && name.Errors.Any())
                {
                    respuesta.code = 404;
                    respuesta.messsage = "Sin Exito.";
                    return Ok(respuesta);
                }
            }

            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await usersService.UpdateUserAsync(id, user);
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }
    }
}

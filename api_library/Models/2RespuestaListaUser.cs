﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class RespuestaListaUser
    {
        public int code { get; set; }

        public string messsage { get; set; }

        public IEnumerable<User> result { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Entities;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class LibraryService : ILibraryService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public LibraryService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "id" };
        }

        public async Task<Library> CreateLibraryAsync(Library newLibrary)
        {
            var libraryEntity = mapper.Map<LibraryEntity>(newLibrary);
            libraryRepository.CreateLibrary(libraryEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Library>(libraryEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        public async Task<bool> DeleteLibraryAsync(int id)
        {
            await validateLibrary(id);
            await libraryRepository.DeleteLibraryAsync(id);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }

        public async Task<Library> GetLibraryAsync(int id)
        {
            var library = await libraryRepository.GetLibraryAsync(id);

            if (library == null)
            {
                throw new NotFoundItemException("library not found");
            }

            return mapper.Map<Library>(library);

        }

        public async Task<IEnumerable<Library>> GetLibrarysAsync( string orderBy)
        {
            var orderByLower = orderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {orderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var librarysEntities = await libraryRepository.GetLibrarysAsync(orderByLower);
            return mapper.Map<IEnumerable<Library>>(librarysEntities);
        }

        public async Task<Library> UpdateLibraryAsync(int id, Library library)
        {
            if (id != library.id)
            {
                throw new InvalidOperationException("URL id needs to be the same as Author id");
            }
            await validateLibrary(id);

            library.id = id;
            var libraryEntity = mapper.Map<LibraryEntity>(library);
            libraryRepository.UpdateLibrary(libraryEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Library>(libraryEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        private async Task<LibraryEntity> validateLibrary(int id)
        {
            var library = await libraryRepository.GetLibraryAsync(id);
            if (library == null)
            {
                throw new NotFoundItemException($"cannot found library with id {id}");
            }

            return library;
        }

    }
}

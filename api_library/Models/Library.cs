﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class Library
    {
        public int? id { get; set; }
        [Required]

        public string name { get; set; }
        [Required]

        public string ubication { get; set; }
        [Required]

        public string description { get; set; }

        public virtual ICollection<Product> list_of_Products { get; set; }
    }
}

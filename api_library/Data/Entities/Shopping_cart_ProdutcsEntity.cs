﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Entities
{
    public class Shopping_cart_ProdutcsEntity
    {
        public int productID { get; set; }
        public ProductEntity product { get; set; }
        public string productName { get; set; }
        public int cartID { get; set; }
        public Shopping_cartEntity shopping_Cart { get; set; }
        public int cantidad { get; set; }
        public int costUnidad { get; set; }
        public int costTotal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Entities;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class UsersService : IUsersService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public UsersService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "lastname", "age", "id" };
        }

        public async Task<User> CreateUserAsync(User newUser)
        {
            var userEntity = mapper.Map<UserEntity>(newUser);
            libraryRepository.CreateUser(userEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<User>(userEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        public async Task<bool> DeleteUserAsync(int id)
        {
            await validateUser(id);
            await libraryRepository.DeleteUserAsync(id);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }

        public async Task<User> GetUserAsync(int id)
        {
         
            var user = await libraryRepository.GetUserAsync(id);

            if (user == null)
            {
                throw new NotFoundItemException("author not found");
            }

            return mapper.Map<User>(user);

        }

        public async Task<IEnumerable<User>> GetUserAsync( string orderBy)
        {
            var orderByLower = orderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {orderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var usersEntities = await libraryRepository.GetUsersAsync( orderByLower);
            return mapper.Map<IEnumerable<User>>(usersEntities);
        }

        public async Task<User> UpdateUserAsync(int id, User user)
        {
            if (id != user.id)
            {
                throw new InvalidOperationException("URL id needs to be the same as Author id");
            }
            await validateUser(id);

           user.id = id;
            var userEntity = mapper.Map<UserEntity>(user);
            libraryRepository.UpdateUser(userEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<User>(userEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        private async Task<UserEntity> validateUser(int id)
        {
            var author = await libraryRepository.GetUserAsync(id);
            if (author == null)
            {
                throw new NotFoundItemException($"cannot found author with id {id}");
            }

            return author;
        }

        public Task<IEnumerable<User>> GetUsersById_rol(int id_rol)
        {
            throw new NotImplementedException();
        }
        public async Task<bool> findUserByEmailPassword(string email, string password)
        {
            var users = await libraryRepository.GetUsersAsync();

            if (users.ToList().Exists(u => u.email == email))
            {
                if (users.ToList().Find(u => u.email == email).password == password)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using api_library.Data.Entities;
using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Repository
{
    public interface ILibraryRepository
    {
        Task<bool> SaveChangesAsync();


        //Librarys
        Task<LibraryEntity> GetLibraryAsync(int id);
        Task<IEnumerable<LibraryEntity>> GetLibrarysAsync(string orderBy = "id");
        Task DeleteLibraryAsync(int id);
        void UpdateLibrary(LibraryEntity Library);
        void CreateLibrary(LibraryEntity Library);


        //Products
        Task<IEnumerable<ProductEntity>> GetProducts(string orderBy = "id");
        Task<ProductEntity> GetProductAsync(int id);
        void CreateProduct(ProductEntity product);
        void UpdateProduct(ProductEntity product);
        Task DeleteProduct(int id);


        //Rols
        Task<RolEntity> GetRolAsync(int id);
        Task<IEnumerable<RolEntity>> GetRolsAsync( string orderBy = "id");
        Task DeleteRolAsync(int id);
        void UpdateRol(RolEntity rol);
        void CreateRol(RolEntity rol);



        //Shopping_carts
        Task<IEnumerable<Shopping_cartEntity>> GetShopping_carts(string orderBy = "id");
        Task<Shopping_cartEntity> GetShopping_cartAsync(int id);
        void CreateShopping_cart(Shopping_cartEntity shopping_cart);
        void UpdateShopping_cart(Shopping_cartEntity shopping_cart);
        Task DeleteShopping_cart(int id);


        //Users
        Task<UserEntity> GetUserAsync(int id);
        Task<IEnumerable<UserEntity>> GetUsersAsync( string orderBy = "id");
        Task DeleteUserAsync(int id);
        void UpdateUser(UserEntity user);
        void CreateUser(UserEntity user);


        //Shopping_cart_Produtcs
        Task<Shopping_cart_ProdutcsEntity> Get_SC_P_By_SC_P_ID(int idSC, int idP);
        Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SC_P_By_SCID(int idSC);
        Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SC_P_By_PID(int idP);
        Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SCP();
        Task Delete_SCP_By_SCID(int idSC);
        Task Delete_P_By_scp(Shopping_cart_ProdutcsEntity scp);
        void Update_cantProductSCP(Shopping_cart_ProdutcsEntity scp, int cant);
        Task<Shopping_cart_ProdutcsEntity> Create_SCP(int idSC, int idP);
    }
}

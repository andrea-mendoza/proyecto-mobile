﻿using api_library.Exceptions;
using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api/[controller]")]
    public class Shopping_cartsController : ControllerBase
    {

        private IShopping_cartsService shopping_cartsService;

        public Shopping_cartsController(IShopping_cartsService shopping_cartsService)
        {
            this.shopping_cartsService = shopping_cartsService;
        }

        [HttpGet]
        public async Task<ActionResult> GetShopping_carts(string orderBy = "id")
        {
            var respuesta = new RespuestaListaShopping_cart();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await shopping_cartsService.GetShopping_cartsAsync(orderBy);
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetShopping_cartAsync(int id)
        {
            var respuesta = new RespuestaShopping_cart();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await shopping_cartsService.GetShopping_cartAsync(id);
                return Ok(respuesta);

            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }

        }
        [HttpPost]
        public async Task<IActionResult> PostShopping_cart([FromBody] Shopping_cart shopping_cart)
        {
            var respuesta = new RespuestaShopping_cart();
             
            respuesta.code = 200;
            respuesta.messsage = "Exito";
            respuesta.result = await shopping_cartsService.CreateShopping_cartAsync(shopping_cart);

            return Ok(respuesta);
        }
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<bool>> DeleteShopping_cart(int Id)
        {
            var respuesta = new RespuestaBoolean();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await this.shopping_cartsService.DeleteShopping_cartAsync(Id);
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutShopping_cart(int id, [FromBody]Shopping_cart shopping_cart)
        {
            var respuesta = new RespuestaShopping_cart();
            if (!ModelState.IsValid)
            {
                var nameshopping_cart = ModelState[nameof(shopping_cart.id_user)];
                if (nameshopping_cart != null && nameshopping_cart.Errors.Any())
                {
                    respuesta.code = 404;
                    respuesta.messsage = "Sin Exito, no encontrado";
                    return Ok(respuesta);
                }
            }

            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result=(await shopping_cartsService.UpdateShopping_cartAsync(id, shopping_cart));
                return Ok(respuesta);
            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado";
                return Ok(respuesta);
            }
        }

        [HttpPost("{id:int}")]
        public async Task<IActionResult> AddProductToShopping_cart(int id, [FromBody] Product product)
        {
            var respuesta = new RespuestaShopping_cart();
            if (!ModelState.IsValid)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, llene los espacios correctamente.";
            }
            else
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await shopping_cartsService.AddProductToShopping_cartAsync(id,product);
            }
            return Ok(respuesta);
        }
    }
}

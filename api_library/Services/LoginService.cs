using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Models;

namespace api_library.Services
{
    public class LoginService : ILoginService
    {
        private IUsersService usersService;

        public LoginService(IUsersService _usersService)
        {
            this.usersService = _usersService;
        }

        public string decoder(string code)
        {
            string extractBtoa = code.Split(" ")[1];
            byte[] data = System.Convert.FromBase64String(extractBtoa);
            string decode = System.Text.ASCIIEncoding.ASCII.GetString(data);
            return decode;
        }

        public Task<bool> verify(string decode)
        {
            string Email;
            string Password;
            if (decode.Contains(":"))
            {
                string[] dataDecode = decode.Split(":");

                Email = dataDecode[0];
                Password = dataDecode[1];

                return usersService.findUserByEmailPassword(Email, Password);
            }
            return usersService.findUserByEmailPassword("falso", "falso");
        }

    }
}

﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface IProductsService
    {
        Task<IEnumerable<Product>> GetProductsAsync( string orderBy);
        Task<Product> GetProductAsync(int id);

        Task<Product> CreateProductAsync(int idLibrary, Product newProduct);

        Task<bool> DeleteProductAsync(int id);
        Task<Product> UpdateProductAsync(int id, Product newProduct);

        Task<IEnumerable<Product>> GetProductsByLibraryId(int idLibrary);
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data;
using api_library.Data.Repository;
using api_library.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace api_library
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IProductsService, ProductsService>();
            services.AddTransient<ILibraryService, LibraryService>();
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IShopping_cartsService, Shopping_cartsService>();
            services.AddTransient<IShopping_cart_ProdutcsService, Shopping_cart_ProdutcsService>();
            services.AddTransient<ILibraryRepository, LibraryRepository>();
            //entity framework config
            services.AddEntityFrameworkSqlServer();
            services.AddDbContext<LibraryDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("GeekLibraryApiDatabase")
                    )
            );
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
            //automapper configuration
            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface ILibraryService
    {
        Task<IEnumerable<Library>>GetLibrarysAsync( string orderBy);
        Task<Library> GetLibraryAsync(int id);
        Task<Library> CreateLibraryAsync(Library newLibrary);
        Task<bool> DeleteLibraryAsync(int id);
        Task<Library> UpdateLibraryAsync(int id, Library newLibrary);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class Shopping_cart_Produtcs
    {
        public int productID { get; set; }
        public Product product { get; set; }
        public string productName { get; set; }
        public int cartID { get; set; }
        public Shopping_cart shopping_Cart { get; set; }
        public int cantidad { get; set; }
        public int costUnidad { get; set; }
        public int costTotal { get; set; }
    }
}

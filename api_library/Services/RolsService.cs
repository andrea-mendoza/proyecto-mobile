﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class RolsService : IRolsService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public RolsService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
        }
        public async Task<Rol> GetRolAsync(int id)
        {
           
            var rol = await libraryRepository.GetRolAsync(id);

            if (rol == null)
            {
                throw new NotFoundItemException("author not found");
            }

            return mapper.Map<Rol>(rol);

        }

        public async Task<IEnumerable<Rol>> GetRolsAsync( string orderBy)
        {
            var orderByLower = orderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {orderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var rolsEntities = await libraryRepository.GetRolsAsync(orderByLower);
            return mapper.Map<IEnumerable<Rol>>(rolsEntities);
        }
    }
}

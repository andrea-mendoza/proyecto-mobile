﻿using api_library.Data.Entities;
using api_library.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Repository
{
    public class LibraryRepository:ILibraryRepository
    {
        private LibraryDbContext libraryDbContext;
        public LibraryRepository(LibraryDbContext libraryDbContext)
        {
            this.libraryDbContext = libraryDbContext;

        }

        public void CreateLibrary(LibraryEntity library)
        {
            libraryDbContext.Librarys.Add(library);
        }

        public void CreateProduct(ProductEntity product)
        {
            libraryDbContext.Products.Add(product);
        }

        public void CreateRol(RolEntity rol)
        {
            libraryDbContext.Rols.Add(rol);
        }

        public void CreateShopping_cart(Shopping_cartEntity shopping_cart)
        {
            libraryDbContext.Shopping_Carts.Add(shopping_cart);
        }

        public void CreateUser(UserEntity user)
        {
            libraryDbContext.Users.Add(user);
        }

        public async Task DeleteLibraryAsync(int id)
        {
            var libraryToDelete = await libraryDbContext.Librarys.SingleAsync(a => a.id == id);
            libraryDbContext.Librarys.Remove(libraryToDelete);
        }

        public async Task  DeleteProduct(int id)
        {
            var productToDelete = await libraryDbContext.Products.SingleAsync(a => a.id == id);
            libraryDbContext.Products.Remove(productToDelete);
        }

        public async Task DeleteRolAsync(int id)
        {
            var rolToDelete = await libraryDbContext.Rols.SingleAsync(a => a.id == id);
            libraryDbContext.Rols.Remove(rolToDelete);
        }

        public async Task DeleteShopping_cart(int id)
        {
            var shopping_cart_ToDelete = await libraryDbContext.Shopping_Carts.SingleAsync(a => a.id == id);
            libraryDbContext.Shopping_Carts.Remove(shopping_cart_ToDelete);
        }

        public async Task DeleteUserAsync(int id)
        {
            var userToDelete = await libraryDbContext.Users.SingleAsync(a => a.id == id);
            libraryDbContext.Users.Remove(userToDelete);
        }

        public async Task<LibraryEntity> GetLibraryAsync(int id)
        {
           
            IQueryable<LibraryEntity> query = libraryDbContext.Librarys;
            query = query.AsNoTracking();
            return await query.SingleOrDefaultAsync(a => a.id == id);
        }

        public async Task<IEnumerable<LibraryEntity>> GetLibrarysAsync( string orderBy)
        {
            IQueryable<LibraryEntity> query = libraryDbContext.Librarys;
            switch (orderBy)
            {
                case "name":
                    query = query.OrderBy(a => a.name);
                    break;
                default:
                    query = query.OrderBy(a => a.id);
                    break;
            }
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }

        public Task<ProductEntity> GetProductAsync(int id)
        {
            IQueryable<ProductEntity> query = libraryDbContext.Products;
            query = query.AsNoTracking();
            return query.SingleAsync(b => b.id == id);
        }

        public async Task<IEnumerable<ProductEntity>> GetProducts(string orderBy)
        {
            IQueryable<ProductEntity> query = libraryDbContext.Products;
            switch (orderBy)
            {
                case "name":
                    query = query.OrderBy(a => a.name);
                    break;
                default:
                    query = query.OrderBy(a => a.id);
                    break;
            }
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }

        public Task<RolEntity> GetRolAsync(int id)
        {
            IQueryable<RolEntity> query = libraryDbContext.Rols;
            query = query.AsNoTracking();
            return query.SingleAsync(b => b.id == id);
        }

        public async Task<IEnumerable<RolEntity>> GetRolsAsync(string orderBy = "id")
        {
            IQueryable<RolEntity> query = libraryDbContext.Rols;
            switch (orderBy)
            {
                case "name":
                    query = query.OrderBy(a => a.name);
                    break;
                default:
                    query = query.OrderBy(a => a.id);
                    break;
            }
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }

        public Task<Shopping_cartEntity> GetShopping_cartAsync(int id)
        {
            IQueryable<Shopping_cartEntity> query = libraryDbContext.Shopping_Carts;
            query = query.AsNoTracking();
            return query.SingleAsync(b => b.id == id);
        }

        public async Task< IEnumerable<Shopping_cartEntity>>GetShopping_carts(string orderBy)
        {
            IQueryable<Shopping_cartEntity> query = libraryDbContext.Shopping_Carts;
            switch (orderBy)
            {
                default:
                    query = query.OrderBy(a => a.id);
                    break;
            }
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }

        public Task<UserEntity> GetUserAsync(int id)
        {
            IQueryable<UserEntity> query = libraryDbContext.Users;
            query = query.AsNoTracking();
            return query.SingleAsync(b => b.id == id);
        }

        public async Task<IEnumerable<UserEntity>> GetUsersAsync(string orderBy = "id")
        {
            IQueryable<UserEntity> query = libraryDbContext.Users;
            switch (orderBy)
            {
                case "name":
                    query = query.OrderBy(a => a.name);
                    break;
                default:
                    query = query.OrderBy(a => a.id);
                    break;
            }
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await libraryDbContext.SaveChangesAsync()) > 0;
        }

        public void UpdateLibrary(LibraryEntity library)
        {
       
            libraryDbContext.Librarys.Update(library);
        }

        public void UpdateProduct(ProductEntity product)
        {
            libraryDbContext.Products.Update(product);
        }

        public void UpdateRol(RolEntity rol)
        {
            libraryDbContext.Rols.Update(rol);
        }

        public void UpdateShopping_cart(Shopping_cartEntity shopping_cart)
        {
            libraryDbContext.Shopping_Carts.Update(shopping_cart);
        }

        public void UpdateUser(UserEntity user)
        {
            libraryDbContext.Users.Update(user);
        }


        //Shopping_cart_Produtcs
        public async Task<Shopping_cart_ProdutcsEntity> Get_SC_P_By_SC_P_ID(int idSC, int idP)
        {
            IQueryable<Shopping_cart_ProdutcsEntity> query = libraryDbContext.Shopping_Cart_Produtcs.Where(scp => scp.cartID == idSC && scp.productID == idP);
            query = query.AsNoTracking();
            return await query.SingleAsync();
        }
        public async Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SC_P_By_SCID(int idSC)
        {
            IQueryable<Shopping_cart_ProdutcsEntity> query = libraryDbContext.Shopping_Cart_Produtcs.Where(scp => scp.cartID == idSC);
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }
        public async Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SC_P_By_PID(int idP)
        {
            IQueryable<Shopping_cart_ProdutcsEntity> query = libraryDbContext.Shopping_Cart_Produtcs.Where(scp => scp.productID == idP);
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }
        public async Task<IEnumerable<Shopping_cart_ProdutcsEntity>> Get_SCP()
        {
            IQueryable<Shopping_cart_ProdutcsEntity> query = libraryDbContext.Shopping_Cart_Produtcs;
            query = query.AsNoTracking();
            return await query.ToArrayAsync();
        }
        public async Task Delete_SCP_By_SCID(int idSC)
        {
            var scpsToDelete = await Get_SC_P_By_SCID(idSC);
            foreach (var x in scpsToDelete.ToList())
            {
                libraryDbContext.Shopping_Cart_Produtcs.Remove(x);
            }
        }
        public async Task Delete_P_By_scp(Shopping_cart_ProdutcsEntity scp)
        {
            libraryDbContext.Shopping_Cart_Produtcs.Remove(scp);
        }
        public void Update_cantProductSCP(Shopping_cart_ProdutcsEntity scp, int cant)
        {
            scp.cantidad = scp.cantidad + cant;
            scp.costTotal = scp.costTotal + (scp.costUnidad * cant);
            libraryDbContext.Shopping_Cart_Produtcs.Update(scp);
        }
        public async Task<Shopping_cart_ProdutcsEntity> Create_SCP(int idSC, int idP)
        {
            Shopping_cart_ProdutcsEntity scp = new Shopping_cart_ProdutcsEntity();
            scp.cartID = idSC;
            scp.productID = idP;
            scp.costTotal = 0;
            scp.cantidad = 0;
            scp.costUnidad = GetProductAsync(idP).Result.price;
            scp.productName = GetProductAsync(idP).Result.name;
            libraryDbContext.Shopping_Cart_Produtcs.Add(scp);
            return scp;
        }
    }   
}

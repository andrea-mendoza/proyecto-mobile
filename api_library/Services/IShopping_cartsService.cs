﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Services
{
    public interface IShopping_cartsService
    {
        Task<IEnumerable<Shopping_cart>> GetShopping_cartsAsync( string orderBy);
        Task<Shopping_cart> GetShopping_cartAsync(int id);
        Task<Shopping_cart> CreateShopping_cartAsync(Shopping_cart newShopping_cart);
        Task<bool> DeleteShopping_cartAsync(int id);
        Task<Shopping_cart> UpdateShopping_cartAsync(int id, Shopping_cart newShopping_cart);


        Task<Shopping_cart> AddProductToShopping_cartAsync(int idShopping_Cart, Product product);
        Task<bool> DeleteProductToShopping_cartAsync(int idShopping_Cart, int idProduct);

    }
}

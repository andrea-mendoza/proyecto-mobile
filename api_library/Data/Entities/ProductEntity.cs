﻿using api_library.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Entities
{
    public class ProductEntity
    {
        [Key]
        [Required]

        public int? id { get; set; }
        [Required]

        public string name { get; set; }
        public int price { get; set; }
        public string photo { get; set; }
        public int quatity { get; set; }
        public bool state { get; set; }
        public int id_library { get; set; }

        public ICollection<Shopping_cart_ProdutcsEntity> shopping_Cart_Produtcs { get; set; }

    }
}

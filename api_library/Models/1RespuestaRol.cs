﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class RespuestaRol
    {
        public int code { get; set; }

        public string messsage { get; set; }

        public Rol result { get; set; }
    }
}
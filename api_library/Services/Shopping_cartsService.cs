﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_library.Data.Entities;
using api_library.Data.Repository;
using api_library.Exceptions;
using api_library.Models;
using AutoMapper;

namespace api_library.Services
{
    public class Shopping_cartsService : IShopping_cartsService
    {
        private HashSet<string> allowedOrderByValues;
        private ILibraryRepository libraryRepository;
        private readonly IMapper mapper;
        public Shopping_cartsService(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "lastname", "age", "id" };
        }

        public async Task<Shopping_cart> CreateShopping_cartAsync(Shopping_cart newShopping_cart)
        {
            var shopping_cartEntity = mapper.Map<Shopping_cartEntity>(newShopping_cart);
            libraryRepository.CreateShopping_cart(shopping_cartEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Shopping_cart>(shopping_cartEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        public async Task<bool> DeleteShopping_cartAsync(int id)
        {
            await validateShopping_cart(id);
            await libraryRepository.DeleteShopping_cart(id);
            if (await libraryRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;
        }

        public async Task<Shopping_cart> GetShopping_cartAsync(int id)
        {
          
            var shopping_cart = await libraryRepository.GetShopping_cartAsync(id);

            if (shopping_cart == null)
            {
                throw new NotFoundItemException("author not found");
            }

            return mapper.Map<Shopping_cart>(shopping_cart);

        }

        public async Task<IEnumerable<Shopping_cart>> GetShopping_cartsAsync( string orderBy)
        {
            var orderByLower = orderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {orderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var shopping_cartsEntities = await libraryRepository.GetShopping_carts(orderByLower);
            return mapper.Map<IEnumerable<Shopping_cart>>(shopping_cartsEntities);
        }

        public async Task<Shopping_cart> UpdateShopping_cartAsync(int id, Shopping_cart shopping_cart)
        {
            if (id != shopping_cart.id)
            {
                throw new InvalidOperationException("URL id needs to be the same as Shopping_cart id");
            }
            await validateShopping_cart(id);

            shopping_cart.id = id;
            var shopping_cartEntity = mapper.Map<Shopping_cartEntity>(shopping_cart);
            libraryRepository.UpdateShopping_cart(shopping_cartEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Shopping_cart>(shopping_cartEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        private async Task<Shopping_cartEntity> validateShopping_cart(int id)
        {
            var shopping_cart = await libraryRepository.GetShopping_cartAsync(id);
            if (shopping_cart == null)
            {
                throw new NotFoundItemException($"cannot found Shopping_cart with id {id}");
            }

            return shopping_cart;
        }

        public async Task<Shopping_cart> AddProductToShopping_cartAsync(int idShopping_Cart, Product product)
        {
            var shopping_cart = await libraryRepository.GetShopping_cartAsync(idShopping_Cart);

            if (shopping_cart == null)
            {
                throw new NotFoundItemException("author not found");
            }

            var cart = mapper.Map<Shopping_cart>(shopping_cart);
            cart.list_of_Products.Add(product);
            cart.total_cost = cart.total_cost + (product.price * product.quatity);

            var shopping_cartEntity = mapper.Map<Shopping_cartEntity>(cart);
            libraryRepository.UpdateShopping_cart(shopping_cartEntity);
            if (await libraryRepository.SaveChangesAsync())
            {
                return mapper.Map<Shopping_cart>(shopping_cartEntity);
            }

            throw new Exception("There were an error with the DB");
        }

        public Task<bool> DeleteProductToShopping_cartAsync(int idShopping_Cart, int idProduct)
        {
            //var shopping_cart = await libraryRepository.GetShopping_cartAsync(id);

            //if (shopping_cart == null)
            //{
            //    throw new NotFoundItemException("author not found");
            //}

            //var cart = mapper.Map<Shopping_cart>(shopping_cart);
            //cart.list_of_Products.ToList().Add(product);

            //var shopping_cartEntity = mapper.Map<Shopping_cartEntity>(cart);
            //libraryRepository.UpdateShopping_cart(shopping_cartEntity);
            //if (await libraryRepository.SaveChangesAsync())
            //{
            //    return mapper.Map<Shopping_cart>(shopping_cartEntity);
            //}

            throw new Exception("There were an error with the DB");
        }
    }
}

﻿using api_library.Exceptions;
using api_library.Models;
using api_library.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Controllers
{
    [Route("api/[controller]")]
    public class RolsController:ControllerBase
    {

        private IRolsService rolsService;

        public RolsController(IRolsService rolsService)
        {
            this.rolsService = rolsService;
        }

        [HttpGet]
        public async Task<ActionResult> GetRols(string orderBy = "id")
        {
            var respuesta = new RespuestaListaRol();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result =(await rolsService.GetRolsAsync(orderBy));
                return Ok(respuesta);
            }
            catch (BadRequestOperationException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
            catch (Exception)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito.";
                return Ok(respuesta);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRolAsync(int id)
        {
            var respuesta = new RespuestaRol();
            try
            {
                respuesta.code = 200;
                respuesta.messsage = "Exito";
                respuesta.result = await rolsService.GetRolAsync(id);
                return Ok(respuesta);

            }
            catch (NotFoundItemException ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.code = 404;
                respuesta.messsage = "Sin Exito, no encontrado.";
                return Ok(respuesta);
            }

        }
    }
}

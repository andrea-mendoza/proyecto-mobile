﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class Shopping_cart
    {
        [Key]
        [Required]

        public int? id { get; set; }
        [Required]

        public int id_user { get; set; }
        public IList<Product> list_of_Products { get; set; }
        public int total_cost { get; set; }
    }
}

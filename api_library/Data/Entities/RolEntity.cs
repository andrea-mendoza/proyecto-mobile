﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data.Entities
{
    public class RolEntity
    {
        [Key]
        [Required]
        public int? id { get; set; }
        [Required]
        public string name { get; set; }
    }
}

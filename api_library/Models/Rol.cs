﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Models
{
    public class Rol
    {
        public int? id { get; set; }
        [Required]

        public string name { get; set; }
    }
}

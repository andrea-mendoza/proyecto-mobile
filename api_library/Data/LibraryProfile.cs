﻿using api_library.Data.Entities;
using api_library.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_library.Data
{ 
    public class LibraryProfile :Profile
    {
        public LibraryProfile()
        {
            this.CreateMap<LibraryEntity, Library>()
                .ReverseMap();

            this.CreateMap<ProductEntity, Product>()
                .ReverseMap();
            this.CreateMap<RolEntity, Rol>()
                .ReverseMap();

            this.CreateMap<Shopping_cartEntity, Shopping_cart>()
                .ReverseMap();
            this.CreateMap<UserEntity, User>()
                .ReverseMap();

            this.CreateMap<Shopping_cart_ProdutcsEntity, Shopping_cart_Produtcs>()
                .ReverseMap();
        }
    }
}
